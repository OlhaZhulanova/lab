const express = require('express');
const dir = './uploadedFiles';
const validator = require('./paramsValidator');
const { 
    createFile, 
    saveFile,
    getFiles,
    getFile,
    deleteFile
} = require('./fsMethods');

const app = express();
app.use(express.json());

app.use('*', (req, res, next) => {
    var invalidParams = validator.validate(req);

    if(invalidParams.length == 0)
        next();
    else {
        res.status(400);
        console.log(invalidParams);
        var response = {message: []};
        invalidParams.map(paramName => response.message.push(`Please specify \'${paramName}\' parameter`));
        res.json(response).end();
    }
})

app.post('/api/files', (req, res) => {
    console.log(req.body);

    createFile(dir, req.body.filename, req.body.content)
        .then(() => res.status(200).json({message: 'File created successfully'}).end())
        .catch(() => res.status(400).json({message: 'File already exists'}).end());
    
});

app.get('/api/files', (req, res) => {
    console.log(req);

    getFiles(dir)
        .then(files => {
            res.status(200).json({message: 'Success', files: files}).end();
        });
        
})

app.get('/api/files/:filename', (req, res) => {
    console.log(req.params.filename);

    getFile(dir, req.params.filename)
        .then(data => res.status(200).json({ message: 'Success', ...data }).end())
        .catch(() => {
            res.status(400).json({message: `No file with '${req.params.filename}' filename found`}).end();
        })
})

app.delete('/api/files/:filename', (req, res) => {
    console.log(req);

    deleteFile(dir, req.params.filename)
        .then(() => res.status(200).json({ message: 'Success'}).end())
        .catch(() => {
            res.status(400).json({message: `No file with '${req.params.filename}' filename found`}).end();
        });
})

app.put('/api/files/:filename', (req, res) => {
    saveFile(dir, req.body.filename, req.body.modifiedContent)
        .then(res.status(200).json({ message: 'Success'}).end())
        .catch(() => {
            res.status(400).json({message: `No file with '${req.params.filename}' filename found`}).end();
        });
})

app.listen(8080);