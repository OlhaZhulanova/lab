function checkParams(requiredParams, actualParams) {
    var invalidParams = [];
    
    requiredParams.map(paramName => {
        if(actualParams[paramName] === undefined){
            invalidParams.push(paramName);
        }
        else if(actualParams[paramName].length == 0){
            invalidParams.push(paramName);
        }
    });

    return invalidParams;
}

function validateCreation(req) {
    var requiredParams = ['filename', 'content'];

    return checkParams(requiredParams, 
        {
            filename: req.body.filename,
            content: req.body.content 
        });
}

function validateModification(req) {
    var requiredParams = ['filename', 'modifiedContent'];

    return checkParams(requiredParams, 
        {
            filename: req.body.filename,
            modifiedContent: req.body.modifiedContent 
        });
}

function validateDeletion(req) {
    var requiredParams = ['filename'];

    return checkParams(requiredParams, 
        {
            filename: req.params[0].split('/')[3]
        });

}

function validate(req) {
    switch(req.method) {
        case 'POST': return validateCreation(req);
        case 'PUT': return validateModification(req);
        case 'DELETE': return validateDeletion(req);
    }

    return [];
}

module.exports = { 
    validate
 };