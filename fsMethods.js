const fs = require('fs');
const fsp = require('fs').promises;
const path = require('path');

function getFiles(dir) {
    return fsp.readdir(dir);
}

async function getFile(dir, filename) {
    var stat = await fsp.stat(path.join(dir, filename));

    return fsp.access(path.join(dir, filename))
        .then(() => fsp.readFile(path.join(dir, filename), 'utf8'))
        .then(content => {
            return {
                filename: filename,
                content: content,
                extension: path.extname(path.join(dir, filename)).substring(1),
                uploadedDate: stat.birthtime.toISOString()
            }
        });
}

async function createFile(dir, filename, content) {
    await fsp.access(dir)
        .catch(() => fs.mkdir(dir, console.log));

    var exists = fs.existsSync(path.join(dir, filename));

    if(exists)
        return Promise.reject();
    else 
        return fsp.writeFile(path.join(dir, filename), content);
}

function saveFile(dir, filename, content) {
    var exists = fs.existsSync(path.join(dir, filename));
    console.log(exists);

    if(!exists)
        return Promise.reject();
    else 
        return fsp.writeFile(path.join(dir, filename), content);
}

function deleteFile(dir, filename) {
    return fsp.unlink(path.join(dir, filename));
}

module.exports = { 
    createFile,
    saveFile, 
    getFiles,
    getFile,
    deleteFile
};